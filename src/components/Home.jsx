import React, { Component } from "react";
import { Link } from "react-router-dom";
import "../assets/scss/home.scss";

class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      login_href: "/login",
      register_href: "/register",
    };
  }

  render() {
    return (
      <div>
        <h1>Welcome to home</h1>
        <Link to={this.state.login_href}>Login</Link>
        <br />
        <Link to={this.state.register_href}>Register</Link>
      </div>
    );
  }
}

export default Home;
