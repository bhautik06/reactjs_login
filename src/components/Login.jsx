import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import validator from "validator";
import '../assets/scss/login.scss'

function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [emailErr, setEmailErr] = useState({});
  const [passwordErr, setPasswordErr] = useState({});
  const history = useHistory();

  const handleSubmit = (e) => {
    e.preventDefault();
    const isValid = formValidation();
    if (isValid) {
      history.push({
        pathname: "/dashboard",
      });
    }
  };

  const formValidation = () => {
    const emailErr = {};
    const passwordErr = {};

    let isValid = true;


    if (!email) {
      emailErr.errMsg = "Email is required";
      isValid = false;
    } else if (!validator.isEmail(email)) {
      emailErr.errMsg = "Please Enter Valid Email.";
      isValid = false;
    } else if (email !== 'abc@gmail.com') {
      emailErr.errMsg = "User does not exists.";
      isValid = false;
    }

    if (!password) {
      passwordErr.errMsg = "Password is required.";
      isValid = false;
    } else if (password !== '12345') {
      passwordErr.errMsg = "Password is Incorrect.";
      isValid = false;
    }

    setEmailErr(emailErr);
    setPasswordErr(passwordErr);
    return isValid;
  };

  return (
    <>
      <h1>Login</h1>
      <form onSubmit={handleSubmit}>
        <div>
          <label>Email: </label>
          <input
            type="text"
            name="email"
            value={email}
            onChange={(e) => {
              setEmail(e.target.value);
            }}
          />
          {Object.keys(emailErr).map((key) => {
            return <div style={{ color: "red" }}>{emailErr[key]}</div>;
          })}
        </div>
        <div>
          <label>Password: </label>
          <input
            type="password"
            name="password"
            value={password}
            onChange={(e) => {
              setPassword(e.target.value);
            }}
          />
          {Object.keys(passwordErr).map((key) => {
            return <div style={{ color: "red" }}>{passwordErr[key]}</div>;
          })}
        </div>
        <button type="submit">Login</button>
      </form>
    </>
  );
}

export default Login;
