import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import validator from "validator";
import "../assets/scss/register.scss";

function Register() {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [unameErr, setUnameerr] = useState({});
  const [emailErr, setEmailErr] = useState({});
  const [passwordErr, setPasswordErr] = useState({});
  const history = useHistory();

  const handleSubmit = (e) => {
    e.preventDefault();
    const isValid = formValidation();
    if (isValid) {
      // console.log({ username, email, password });
      history.push({
        pathname: "/dashboard",
      });
    }
  };

  const formValidation = () => {
    const unameErr = {};
    const emailErr = {};
    const passwordErr = {};

    let isValid = true;

    if (!username) {
      unameErr.errMsg = "Username is required";
      isValid = false;
    }

    if (!email) {
      emailErr.errMsg = "Email is required";
      isValid = false;
    } else if (!validator.isEmail(email)) {
      emailErr.errMsg = "Please Enter Valid Email.";
      isValid = false;
    }

    if (!password) {
      passwordErr.errMsg = "Password is required.";
      isValid = false;
    } else if (!validator.isLength(password, { min: 4, max: 8 })) {
      passwordErr.errMsg = "Password must be at least 4 - 8 characters.";
      isValid = false;
    }

    setUnameerr(unameErr);
    setEmailErr(emailErr);
    setPasswordErr(passwordErr);
    return isValid;
  };

  return (
    <>
      <h1>Register</h1>
      <form onSubmit={handleSubmit}>
        <div>
          <label>Username: </label>
          <input
            type="text"
            name="username"
            value={username}
            onChange={(e) => {
              setUsername(e.target.value);
            }}
          />
          {Object.keys(unameErr).map((key) => {
            return <div style={{ color: "red" }}>{unameErr[key]}</div>;
          })}
        </div>
        <div>
          <label>Email: </label>
          <input
            type="text"
            name="email"
            value={email}
            onChange={(e) => {
              setEmail(e.target.value);
            }}
          />
          {Object.keys(emailErr).map((key) => {
            return <div style={{ color: "red" }}>{emailErr[key]}</div>;
          })}
        </div>
        <div>
          <label>Password: </label>
          <input
            type="password"
            name="password"
            value={password}
            onChange={(e) => {
              setPassword(e.target.value);
            }}
          />
          {Object.keys(passwordErr).map((key) => {
            return <div style={{ color: "red" }}>{passwordErr[key]}</div>;
          })}
        </div>
        <button type="submit">Sign Up</button>
      </form>
    </>
  );
}

export default Register;
